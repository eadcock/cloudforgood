
<!doctype html>
<html lang="en-US">
    <head>
  <meta charset="UTF-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Process Builder: Tips and Tricks &#8211; C4G dev site</title>
<meta name='robots' content='noindex,follow' />
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="C4G dev site &raquo; Feed" href="http://localhost/C4G/feed/" />
<link rel="alternate" type="application/rss+xml" title="C4G dev site &raquo; Comments Feed" href="http://localhost/C4G/comments/feed/" />
<link rel="alternate" type="application/rss+xml" title="C4G dev site &raquo; Process Builder: Tips and Tricks Comments Feed" href="http://localhost/C4G/process-builder-tips-and-tricks/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/localhost\/C4G\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.7"}};
			!function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),!(j.toDataURL().length<3e3)&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,65039,8205,55356,57096),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,55356,57096),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55357,56425,55356,57341,8205,55357,56507),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55357,56425,55356,57341,55357,56507),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='dashicons-css'  href='http://localhost/C4G/wp-includes/css/dashicons.min.css?ver=4.7' type='text/css' media='all' />
<link rel='stylesheet' id='admin-bar-css'  href='http://localhost/C4G/wp-includes/css/admin-bar.min.css?ver=4.7' type='text/css' media='all' />
<link rel='stylesheet' id='etch-child-css'  href='http://localhost/C4G/wp-content/themes/etch-child/style.css?ver=4.7' type='text/css' media='all' />
<link rel='stylesheet' id='luxe_main-css'  href='http://localhost/C4G/wp-content/themes/etch/dist/styles/main-aeac9a3cf5.css' type='text/css' media='all' />
<link rel='stylesheet' id='kirki_google_fonts-css'  href='https://fonts.googleapis.com/css?family=Signika%3A300%2Cregular%2C600%7CPlay%3Aregular&#038;subset' type='text/css' media='all' />
<link rel='stylesheet' id='kirki-styles-luxe_options-css'  href='http://localhost/C4G/wp-content/plugins/kirki/assets/css/kirki-styles.css' type='text/css' media='all' />
<style id='kirki-styles-luxe_options-inline-css' type='text/css'>
@media (min-width: 1200px){header.banner .container{max-width:1170px;}.nav-primary.off-canvas{width:300px;}#sidebar-off-canvas{width:300px;}.container{max-width:1170px;}}header.banner{position:absolute;font-family:Signika, Helvetica, Arial, sans-serif;font-weight:300;font-style:normal;font-size:14px;line-height:1;letter-spacing:0;text-transform:uppercase;}header.banner, header.banner .navbar-nav>li, header.banner .brand-wrap, .nav-btn-wrap{height:80px;}body.single-portfolio #content.container, body.single-product #content.container{padding-top:80px;}body.header-position-fixed-bottom{padding-bottom:80px;}.main>.outer-container:first-child>.vc_row{padding-top:80px;}.brand-text{line-height:80px;}.navbar-brand>img{max-height:80px;}.navbar-brand>span, .navbar-brand>img{padding-top:15px;padding-bottom:15px;}.header-dark-active header.banner{background-color:rgba(255,255,255,0);border-width:0px;border-color:#3d3d3d;}.header-dark-active header.banner, .header-dark-active header.banner a, .header-dark-active header.banner i.icon{color:#FFFFFF;}.header-dark-active .nav-btn .nav-icon span{border-color:#FFFFFF;}.header-dark-active header.banner a:hover, .header-dark-active header.banner .nav-btn:hover, .header-dark-active .nav-primary li.current-menu-item a, .nav-hover-effect-underline .nav-primary a:after, .header-dark-active a:hover i.icon{color:#6DB3D6;}.header-dark-active .nav-btn:hover .nav-icon span{border-color:#6DB3D6;}.header-light-active header.banner{background-color:#fff;border-width:0px;border-color:#3d3d3d;}.header-light-active header.banner, .header-light-active header.banner a, .header-light-active header.banner i.icon{color:#6D6E6E;}.header-light-active .nav-btn .nav-icon span{border-color:#6D6E6E;}.header-light-active header.banner a:hover, .header-light-active header.banner .nav-btn:hover, .header-light-active .nav-primary li.current-menu-item a, .header-light-active header.banner a:hover i.icon{color:#0096CA;}.header-light-active .nav-btn:hover .nav-icon span{border-color:#0096CA;}header.banner.header-scrolled{background-color:#fff;border-width:0px;border-color:#3d3d3d;}header.header-scrolled.banner a, .header-scrolled .nav-btn, header.banner.header-scrolled i.icon{color:#6D6E6E;}.header-scrolled .nav-btn .nav-icon span, .header-scrolled a.btn{border-color:#6D6E6E;}header.banner.header-scrolled a:hover, header.banner.header-scrolled .nav-btn:hover, .header-scrolled .nav-primary li.current-menu-item a, .header-scrolled a:hover i.icon{color:#0096CA;}.header-scrolled .nav-btn:hover .nav-icon span{border-color:#0096CA;}header.banner.header-scrolled, header.banner.header-scrolled .navbar-nav>li, header.banner.header-scrolled .brand-wrap, .header-scrolled .nav-btn-wrap{height:91px;}.header-scrolled .navbar-brand>span, .header-scrolled .navbar-brand>img{padding-top:10px;}.header-scrolled .navbar-brand>img{max-height:70px;}.nav-primary.off-canvas .navbar-nav>li{padding-top:2px;padding-bottom:2px;}.nav-primary.off-canvas{background-color:#f7f7f7;}.nav-primary.off-canvas li a{font-family:Play, Helvetica, Arial, sans-serif;font-weight:400;font-style:normal;font-size:14px;line-height:1.5;letter-spacing:0;text-transform:none;color:#FFFFFF;}.nav-primary.off-canvas li a:hover, .nav-side a:hover, .nav-primary.off-canvas li.current-menu-item a{color:#99D5EA;}.nav-primary.off-canvas li a:after{background-color:#99D5EA;}.nav-primary li a:hover{animation-name:none;}#sidebar-off-canvas{background-color:#f7f7f7;}body, .content-wrapper{background-color:#fff;}.border .inner-border{background-color:#3d3d3d;}.border.left .inner-border, .border.right .inner-border{width:0px;}.border.top .inner-border, .border.bottom .inner-border{height:0px;}.border{padding:0px;}body, p{font-family:Play, Helvetica, Arial, sans-serif;font-weight:400;font-style:normal;font-size:1rem;line-height:1.5;letter-spacing:0;text-transform:none;color:#6D6E6E;}a{color:#0096CA;}a:hover{color:#BFBFBF;}h1, .h1{font-family:Signika, Helvetica, Arial, sans-serif;font-weight:300;font-style:normal;font-size:4em;line-height:1.5;letter-spacing:0;text-transform:uppercase;color:#326596;}h2, .h2{font-family:Signika, Helvetica, Arial, sans-serif;font-weight:400;font-style:normal;font-size:32px;line-height:1.5;letter-spacing:0;text-transform:uppercase;color:#326596;}h3, .h3{font-family:Signika, Helvetica, Arial, sans-serif;font-weight:400;font-style:normal;font-size:22px;line-height:1.5;letter-spacing:0;text-transform:uppercase;color:#326596;}h4, .h4, .comment-reply-title{font-family:Signika, Helvetica, Arial, sans-serif;font-weight:600;font-style:normal;font-size:18px;line-height:1.5;letter-spacing:0;text-transform:none;color:#1F2A2C;}h5, .h5{font-family:Signika, Helvetica, Arial, sans-serif;font-weight:400;font-style:normal;font-size:16px;line-height:1.5;letter-spacing:0;text-transform:none;color:#1F2A2C;}.page-header, .page-header a, .page-header h1, .page-header .subtitle, .page-header div{color:#fff;}.page-header{text-align:center;background-color:#1e1e1e;padding-top:200px;padding-bottom:100px;}.vc_btn3.vc_btn3-size-md, .button, .btn.btn-default, .btn.btn-primary, input[type=submit], .vc_btn3-container .vc_btn3, .vc_btn3.vc_btn3-style-theme-default, .vc_btn3.vc_btn3-style-theme-primary, .woocommerce input.button, .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt, .woocommerce a.button, .woocommerce button.button.alt:disabled[disabled], .dummy-keep-at-end-btn{font-family:Signika, Helvetica, Arial, sans-serif;font-weight:300;font-style:normal;font-size:16px;line-height:1;letter-spacing:0;text-transform:uppercase;color:#326596;}.button:hover, .btn.btn-default:hover, .btn.btn-primary:hover, input[type=submit]:hover, .vc_btn3-container .vc_btn3:hover, .vc_btn3.vc_btn3-style-theme-default:hover, .vc_btn3.vc_btn3-style-theme-primary:hover, .woocommerce input.button:hover, .woocommerce #respond input#submit.alt:hover, .woocommerce a.button.alt:hover, .woocommerce button.button.alt:hover, .woocommerce input.button.alt:hover, .woocommerce a.button:hover, .woocommerce button.button.alt:disabled[disabled]:hover, .dummy-keep-at-end-btn{color:#0096CA !important;background-color:rgba(61,61,61,0);border-color:#0096CA;}.button, .btn.btn-default, .btn.btn-primary, input[type=submit], .vc_btn3-container .vc_btn3, .vc_btn3.vc_btn3-style-theme-default, .vc_btn3.vc_btn3-style-theme-primary, .woocommerce input.button, .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt, .woocommerce a.button, .woocommerce button.button.alt:disabled[disabled], .dummy-keep-at-end-btn{background-color:rgba(25,25,25,0);padding-left:50px;padding-right:50px;border-color:#326596;-webkit-border-radius:0px;-moz-border-radius:0px;border-radius:0px;}.button, .btn.btn-default, .btn.btn-primary, input[type=submit], .vc_btn3-container .vc_btn3, .vc_btn3.vc_btn3-style-theme-default, .vc_btn3.vc_btn3-style-theme-primary, .woocommerce input.button, .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt, .woocommerce a.button, .woocommerce button.button.alt:disabled[disabled], .dummy-keep-at-end-btn, input{padding-top:15px !important;padding-bottom:15px !important;}.button, .btn.btn-default, .btn.btn-primary, input[type=submit], .vc_btn3-container .vc_btn3, .vc_btn3.vc_btn3-style-theme-default, .vc_btn3.vc_btn3-style-theme-primary, .woocommerce input.button, .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt, .woocommerce a.button, .woocommerce button.button.alt:disabled[disabled], .dummy-keep-at-end-btn, .button:hover, .btn.btn-default:hover, .btn.btn-primary:hover, input[type=submit]:hover, .vc_btn3-container .vc_btn3:hover, .vc_btn3.vc_btn3-style-theme-default:hover, .vc_btn3.vc_btn3-style-theme-primary:hover, .woocommerce input.button:hover, .woocommerce #respond input#submit.alt:hover, .woocommerce a.button.alt:hover, .woocommerce button.button.alt:hover, .woocommerce input.button.alt:hover, .woocommerce a.button:hover, .woocommerce button.button.alt:disabled[disabled]:hover, .dummy-keep-at-end-btn{border-width:2px; border-style: solid;}.button, input[type=submit], .comment-form input[type=submit], .btn.btn-primary, .vc_btn3.vc_btn3-style-theme-primary, .woocommerce a.button, .woocommerce input.button, .woocommerce button.button.alt:disabled[disabled], .dummy-keep-at-end-btn{background-color:rgba(255,255,255,0);color:#F2F2F2;border-color:#F2F2F2;}.button:hover, input[type=submit]:hover, .comment-form input[type=submit]:hover, .btn.btn-primary:hover, .vc_btn3.vc_btn3-style-theme-primary:hover, .woocommerce a.button:hover, .woocommerce input.button:hover, .woocommerce button.button.alt:disabled[disabled]:hover, .dummy-keep-at-end-btn{background-color:rgba(255,255,255,0);color:#6DB3D6 !important;border-color:#6DB3D6;}input, textarea, select, .dummy-keep-at-end-input{background-color:rgba(226,226,226,0.3);border-color:#3d3d3d;font-family:Play, Helvetica, Arial, sans-serif;font-weight:400;font-style:normal;font-size:14px;line-height:1;letter-spacing:0;text-transform:none;color:#1F2A2C;}input:focus, textarea:focus, select:focus, .dummy-keep-at-end-input{background-color:rgba(226,226,226,0.5);border-color:#3d3d3d;}input, textarea, select, .dummy-keep-at-end-input, input:hover, textarea:hover, select:hover, .dummy-keep-at-end-input{border-width:0px; border-style: solid;}footer.content-info h1, footer.content-info .h1, footer.content-info h2, footer.content-info .h2, footer.content-info h3, footer.content-info .h3, footer.content-info h4, footer.content-info .h4, footer.content-info h5, footer.content-info .h5{color:#FFFFFF;}footer.content-info, footer.content-info p{color:#D3D3D3;}footer.content-info a{color:#E2E2E2;}footer.content-info a:hover{color:#BFBFBF;}footer.content-info{background-color:#6D6E6E;}footer.content-info .footer-wrap{background-color:#ffffffpx;}.footer-widgets-wrap{padding-top:49px;padding-bottom:49px;}.footer-copy{padding-top:30px;padding-bottom:30px;border-top-width:0;border-color:#3d3d3d;}body.archive .page-header, body.archive .page-header a, body.archive .page-header h1, body.archive .page-header .subtitle, body.archive .page-header div{color:#1e1e1e;}body.archive .page-header{background-color:#fff;}.preloader{background-color:#fff;}@media (max-width: 991px){h1, .h1{font-size:3.32em;line-height:;}h2, .h2{font-size:28.448px;line-height:;}h3, .h3{font-size:18.854px;line-height:;}}@media (max-width: 767px){h1, .h1{font-size:2.664em;line-height:;}h2, .h2{font-size:23.04px;line-height:;}h3, .h3{font-size:17.16px;line-height:;}}
</style>
<script type='text/javascript' src='http://localhost/C4G/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='http://localhost/C4G/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<link rel='https://api.w.org/' href='http://localhost/C4G/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://localhost/C4G/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://localhost/C4G/wp-includes/wlwmanifest.xml" /> 
<link rel='prev' title='Make Salesforce Work for You: Workflows and Email Automations' href='http://localhost/C4G/make-salesforce-work-for-you-workflows-and-email-automations/' />
<link rel='next' title='side post test' href='http://localhost/C4G/side-post-test/' />
<meta name="generator" content="WordPress 4.7" />
<link rel="canonical" href="http://localhost/C4G/process-builder-tips-and-tricks/" />
<link rel='shortlink' href='http://localhost/C4G/?p=179' />
<link rel="alternate" type="application/json+oembed" href="http://localhost/C4G/wp-json/oembed/1.0/embed?url=http%3A%2F%2Flocalhost%2FC4G%2Fprocess-builder-tips-and-tricks%2F" />
<link rel="alternate" type="text/xml+oembed" href="http://localhost/C4G/wp-json/oembed/1.0/embed?url=http%3A%2F%2Flocalhost%2FC4G%2Fprocess-builder-tips-and-tricks%2F&#038;format=xml" />
    <style id="theme-metabox-css" type="text/css">
        .page-header {
                            background-image: url('http://localhost/C4G/wp-content/uploads/2016/12/nyc-world-tour-2016.png');                                                                                }
        .page-header .page-header-overlay {
                            background-color: rgba(29,79,144,0.5);
                    }
        .page-header h1, .page-header .subtitle {
                    }
    </style>
    		<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
		<meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="http://localhost/C4G/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><!--[if IE  8]><link rel="stylesheet" type="text/css" href="http://localhost/C4G/wp-content/plugins/js_composer/assets/css/vc-ie8.min.css" media="screen"><![endif]--><style type="text/css" media="print">#wpadminbar { display:none; }</style>
<style type="text/css" media="screen">
	html { margin-top: 32px !important; }
	* html body { margin-top: 32px !important; }
	@media screen and ( max-width: 782px ) {
		html { margin-top: 46px !important; }
		* html body { margin-top: 46px !important; }
	}
</style>
<noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript></head>
    <body class="post-template-default single single-post postid-179 single-format-standard logged-in admin-bar no-customize-support process-builder-tips-and-tricks sidebar-primary content-single-top header-dark-active header-transition-none header-position-absolute header-default nav-effect-slide nav-hover-effect-none wpb-js-composer js-comp-ver-4.12 vc_responsive">
        <div class="hidden-md hidden-lg">
    <nav id="nav-primary" class="nav-primary off-canvas off-canvas-right nav-side" data-offcanvas-position="right" data-move-x="100%" data-move-y="" data-active-class="nav-active" data-content-push="-200px">
    <div class="close-btn toggle-off-canvas" data-offcanvas-target="#nav-primary"><i class="icon ion-close-round"></i></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12"><h4 class="nav-side-title"></h4></div>
            <div class="col-sm-12">
                <div class="menu-main-navigation-container"><ul id="menu-main-navigation" class="navbar-nav nav"><li id="menu-item-47" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-47"><a href="http://localhost/C4G/">Home</a></li>
<li id="menu-item-335" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-335"><a href="http://localhost/C4G/about-us/">About Us</a>
<ul class="sub-menu">
	<li id="menu-item-338" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-338"><a href="http://localhost/C4G/why-we-are-different/">Why We Are Different</a></li>
	<li id="menu-item-337" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-337"><a href="http://localhost/C4G/the-cloud-team/">The Cloud Team</a></li>
	<li id="menu-item-336" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-336"><a href="http://localhost/C4G/about-us/careers/">Careers</a></li>
</ul>
</li>
<li id="menu-item-349" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-349"><a href="http://localhost/C4G/blog/">Resources</a>
<ul class="sub-menu">
	<li id="menu-item-350" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-350"><a href="http://localhost/C4G/blog/">Blog</a></li>
	<li id="menu-item-351" class="menu-item menu-item-type-post_type menu-item-object-success menu-item-351"><a href="http://localhost/C4G/success/success-story-1/">Success Story 1</a></li>
</ul>
</li>
</ul></div>            </div>
            <div class="col-sm-12"><h4 class="nav-side-title"></h4></div>
        </div>
    </div>
</nav>
</div>
<header class="banner header-default" id="header">
  <div class="container">
    <div class="pull-left brand-pull">
            <div class="brand-wrap">
        <a class="brand navbar-brand brand-dark" href="http://localhost/C4G/"><img class="logo-dark" id="logo-dark" src="http://localhost/C4G/wp-content/uploads/2016/12/C4G-logo-darkback_03.png"></a>
        <a class="brand navbar-brand brand-light" href="http://localhost/C4G/"><img class="logo-light" id="logo-light" src="http://localhost/C4G/wp-content/uploads/2016/12/C4G-logo-lightback-1.png"></a>
        <a class="brand navbar-brand brand-scrolled" href="http://localhost/C4G/"><img class="logo-scrolled" id="logo-scrolled" src="http://localhost/C4G/wp-content/uploads/2016/12/C4G-logo-scroll.png"></a>
    </div>
    </div>
    <div class="pull-right">
        <nav class="nav-primary pull-left hidden-xs hidden-sm">
          <div class="menu-main-navigation-container"><ul id="menu-main-navigation-1" class="navbar-nav nav"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-47"><a href="http://localhost/C4G/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-335"><a href="http://localhost/C4G/about-us/">About Us</a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-338"><a href="http://localhost/C4G/why-we-are-different/">Why We Are Different</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-337"><a href="http://localhost/C4G/the-cloud-team/">The Cloud Team</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-336"><a href="http://localhost/C4G/about-us/careers/">Careers</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-349"><a href="http://localhost/C4G/blog/">Resources</a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-350"><a href="http://localhost/C4G/blog/">Blog</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-success menu-item-351"><a href="http://localhost/C4G/success/success-story-1/">Success Story 1</a></li>
</ul>
</li>
</ul></div>        </nav>
        <div class="pull-left">
            <ul class="nav navbar-nav pull-right header-buttons"></ul>        </div>
        <div class="hidden-md hidden-lg">
          <div class="nav-btn-wrap"><div class="toggle-off-canvas nav-btn nav-btn-theme-default" data-offcanvas-target="#nav-primary">Menu</div></div>        </div>
    </div>
  </div>
</header>
<div id="sidebar-off-canvas" class="sidebar off-canvas off-canvas-right sidebar-off-canvas" data-move-x="100%" data-move-y="" data-active-class="sidebar-offcanvas-active" data-content-push="0px">
    </div>
    <div id="preloader" class="preloader loading-fade">
	<div class="preloader-content">
                <!-- <div class="uil-ring-css"><div></div></div> -->
    </div>
    <div class="preloader-icon"></div>
</div>
    <div id="shadow-overlay" class="shadow-overlay"></div>
        <div id="content-wrapper" class="content-wrapper">

                    <div class="page-header outer-container">
                <div class="page-header-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                                                <h1>Process Builder: Tips and Tricks</h1>
                                <div class="post-date h5">22 December 2016</div><div class="post-categories h6"></div>                </div>
            </div>
        </div>
            </div>
        
                <div class="wrap container" role="document" id="content">
                        <div class="row">
                <main class="main">
                    <article class="content-single-top post-179 post type-post status-publish format-standard has-post-thumbnail hentry category-featured category-tools">
    <div class="entry-content">
        <p>If you have never used Process Builder before, you are missing out on one of the more powerful tools to be added to the Salesforce platform in recent years. Much of what Process Builder can do would have taken programming in the past, however, that functionality is now in the hands of the Salesforce admin.</p>
<p>Process Builder has some crossover with Workflow Rules but it’s capabilities can (and do) go beyond the scope of Workflows such as:</p>
<ul>
<li>Creating a Record</li>
<li>Updating records other than the one which started the workflow</li>
<li>Initiating an Approval Process</li>
<li>Initiating a Flow</li>
<li>Initiating Apex</li>
<li>Firing off another Process Builder</li>
</ul>
<p>That’s all great news and plenty to get the #AwesomeAdmin started. After you’ve been working with Process Builder for a while you will come across a few things that work differently than they did in Workflows, or are unique to Process Builder. Here are some <strong>tips and tricks</strong> to keep in mind when working with this tool. Whether you are just starting out with Process Builder or are looking to deepen your knowledge this post is for you.</p>
<h3>Zero Hour Scheduled Action</h3>
<p>Say what? Sometimes a Process Builder action needs to fire off a few minutes after it’s been evaluated to avoid conflicts with other Workflows or code. If you’ve built out a Process Builder <strong>and it keeps throwing errors</strong> (or simply doesn’t seem to work), you may try this method.</p>
<p>Create a Scheduled Action.</p>
    </div>
    <footer>
        <div class="author-meta">
    <div class="row">
        <div class="col-sm-3 col-lg-2">
            <img alt='eadcock' src='http://2.gravatar.com/avatar/509212c064a28864bbb22db0b8367fda?s=96&#038;d=mm&#038;r=g' srcset='http://2.gravatar.com/avatar/509212c064a28864bbb22db0b8367fda?s=192&amp;d=mm&amp;r=g 2x' class='avatar avatar-96 photo' height='96' width='96' /> 
        </div>
        <div class="col-sm-9 col-lg-10">
            <h4>eadcock</h4>
            <div class="author-description">
                            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="social-icons">
                        <ul>
                                                    </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
            </footer>
    
<section id="comments" class="comments">
  
  
  	<div id="respond" class="comment-respond">
		<h3 id="reply-title" class="comment-reply-title">Leave a Reply <small><a rel="nofollow" id="cancel-comment-reply-link" href="/C4G/process-builder-tips-and-tricks/#respond" style="display:none;">Cancel reply</a></small></h3>			<form action="http://localhost/C4G/wp-comments-post.php" method="post" id="commentform" class="comment-form" novalidate>
				<p class="logged-in-as"><a href="http://localhost/C4G/wp-admin/profile.php" aria-label="Logged in as eadcock. Edit your profile.">Logged in as eadcock</a>. <a href="http://localhost/C4G/wp-login.php?action=logout&amp;redirect_to=http%3A%2F%2Flocalhost%2FC4G%2Fprocess-builder-tips-and-tricks%2F&amp;_wpnonce=38ddbffd35">Log out?</a></p><p class="comment-form-comment"><label for="comment">Comment</label> <textarea id="comment" name="comment" cols="45" rows="8" maxlength="65525" aria-required="true" required="required"></textarea></p><p class="form-submit"><input name="submit" type="submit" id="submit" class="submit" value="Post Comment" /> <input type='hidden' name='comment_post_ID' value='179' id='comment_post_ID' />
<input type='hidden' name='comment_parent' id='comment_parent' value='0' />
</p><input type="hidden" id="_wp_unfiltered_html_comment_disabled" name="_wp_unfiltered_html_comment_disabled" value="146c2a2788" /><script>(function(){if(window===window.parent){document.getElementById('_wp_unfiltered_html_comment_disabled').name='_wp_unfiltered_html_comment';}})();</script>
			</form>
			</div><!-- #respond -->
	</section>
</article>
<nav class="post-nav">
    <span class="previous-link h5"><i class="icon ion-ios-arrow-left"></i> <a href="http://localhost/C4G/make-salesforce-work-for-you-workflows-and-email-automations/" rel="prev">Previous</a></span> 
        <span class="next-link h5"><a href="http://localhost/C4G/side-post-test/" rel="next">Next</a> <i class="icon ion-ios-arrow-right"></i></a> 
</nav>
                </main><!-- /.main -->
                  <aside class="sidebar" id="sidebar-primary">
    <div class="widget search-2 widget_search"><form role="search" method="get" class="search-form" action="http://localhost/C4G/">
				<label>
					<span class="screen-reader-text">Search for:</span>
					<input type="search" class="search-field" placeholder="Search &hellip;" value="" name="s" />
				</label>
				<input type="submit" class="search-submit" value="Search" />
			</form></div>		<div class="widget recent-posts-2 widget_recent_entries">		<h3>Recent Posts</h3>		<ul>
					<li>
				<a href="http://localhost/C4G/side-post-test/">side post test</a>
						</li>
					<li>
				<a href="http://localhost/C4G/process-builder-tips-and-tricks/">Process Builder: Tips and Tricks</a>
						</li>
					<li>
				<a href="http://localhost/C4G/make-salesforce-work-for-you-workflows-and-email-automations/">Make Salesforce Work for You: Workflows and Email Automations</a>
						</li>
					<li>
				<a href="http://localhost/C4G/burst-your-silos-and-become-a-connected-nonprofit/">Burst your Silos and Become a Connected Nonprofit</a>
						</li>
					<li>
				<a href="http://localhost/C4G/hello-world/">Meet Us at the Salesforce World Tour in New York</a>
						</li>
				</ul>
		</div>		<div class="widget recent-comments-2 widget_recent_comments"><h3>Recent Comments</h3><ul id="recentcomments"><li class="recentcomments"><span class="comment-author-link"><a href='https://wordpress.org/' rel='external nofollow' class='url'>A WordPress Commenter</a></span> on <a href="http://localhost/C4G/hello-world/#comment-1">Meet Us at the Salesforce World Tour in New York</a></li></ul></div><div class="widget archives-2 widget_archive"><h3>Archives</h3>		<ul>
			<li><a href='http://localhost/C4G/2017/01/'>January 2017</a></li>
	<li><a href='http://localhost/C4G/2016/12/'>December 2016</a></li>
		</ul>
		</div><div class="widget categories-2 widget_categories"><h3>Categories</h3>		<ul>
	<li class="cat-item cat-item-9"><a href="http://localhost/C4G/category/blog/" title="Cloud for Good Blog Entries">Blog</a>
</li>
	<li class="cat-item cat-item-12"><a href="http://localhost/C4G/category/blog/communication/" >Communication</a>
</li>
	<li class="cat-item cat-item-6"><a href="http://localhost/C4G/category/featured/" >Featured</a>
</li>
	<li class="cat-item cat-item-11"><a href="http://localhost/C4G/category/blog/tools/" >Tools</a>
</li>
	<li class="cat-item cat-item-1"><a href="http://localhost/C4G/category/uncategorized/" >Uncategorized</a>
</li>
		</ul>
</div><div class="widget meta-2 widget_meta"><h3>Meta</h3>			<ul>
			<li><a href="http://localhost/C4G/wp-admin/">Site Admin</a></li>			<li><a href="http://localhost/C4G/wp-login.php?action=logout&#038;_wpnonce=38ddbffd35">Log out</a></li>
			<li><a href="http://localhost/C4G/feed/">Entries <abbr title="Really Simple Syndication">RSS</abbr></a></li>
			<li><a href="http://localhost/C4G/comments/feed/">Comments <abbr title="Really Simple Syndication">RSS</abbr></a></li>
			<li><a href="https://wordpress.org/" title="Powered by WordPress, state-of-the-art semantic personal publishing platform.">WordPress.org</a></li>			</ul>
			</div>  </aside><!-- /.sidebar -->
            </div><!-- /.row -->
        </div><!-- /.wrap -->
            </div><!-- /.content-wrapper -->
    <footer class="content-info  footer-fixed" id="footer">
    <div class="footer-widgets-wrap"><div class="container"><div class="row">            <div class="col-sm-6">
                <div class="widget text-6 widget_text"><h3>Contact Us</h3>			<div class="textwidget">Contact us today to learn how we can help you become more effective and efficient. <br/>
1 (855) 536-1251 • info@cloud4good.com

</div>
		</div>            </div>
                        <div class="col-sm-6">
                <div class="widget text-8 widget_text">			<div class="textwidget"><a href="https://www.facebook.com/cloudforgood/" class="social-item" target="_blank"><i class="icon ion-social-facebook"></i></a>

<a href="https://twitter.com/cloud4good?lang=en" class="social-item" target="_blank"><i class="icon ion-social-twitter"></i></a>

<a href="www.pinterest.com/cloud4good/" class="social-item" target="_blank"><i class="icon ion-social-pinterest"></i> </a>

<a href="#" class="social-item" target="_blank"><i class="icon ion-social-rss"></i> </a>

<a href="https://www.youtube.com/user/TFrankfurt" class="social-item" target="_blank"><i class="icon ion-social-youtube"></i></a>

<a href="https://www.linkedin.com/company/cloud-for-good" class="social-item" target="_blank"><i class="icon ion-social-linkedin"></i></a></div>
		</div>            </div>
            </div></div></div>            <div class="footer-copy-wrap">
            <div class="container footer-copy">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="widget text-7 widget_text">			<div class="textwidget">© 2017 Cloud for Good™. All Rights reserved</div>
		</div>                    </div>
                </div>
            </div>
        </div>
        </footer>
<script type='text/javascript' src='http://localhost/C4G/wp-includes/js/admin-bar.min.js?ver=4.7'></script>
<script type='text/javascript' src='http://localhost/C4G/wp-includes/js/comment-reply.min.js?ver=4.7'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var theme = {"headerScrolled":"1","headerScrolledDistance":"500","headerHideAfterScroll":"","headerHideDistance":"500","headerTransition":"hideAfterScroll","headerPosition":"80","headerHeight":"80","headerScrolledHeight":"91","smoothScroll":"","preloader":"0","openCartOnAjaxAdd":"1","fixedFooter":"1"};
/* ]]> */
</script>
<script type='text/javascript' src='http://localhost/C4G/wp-content/themes/etch/dist/scripts/main-330db95318.js'></script>
<script type='text/javascript' src='http://localhost/C4G/wp-includes/js/wp-embed.min.js?ver=4.7'></script>
	<!--[if lte IE 8]>
		<script type="text/javascript">
			document.body.className = document.body.className.replace( /(^|\s)(no-)?customize-support(?=\s|$)/, '' ) + ' no-customize-support';
		</script>
	<![endif]-->
	<!--[if gte IE 9]><!-->
		<script type="text/javascript">
			(function() {
				var request, b = document.body, c = 'className', cs = 'customize-support', rcs = new RegExp('(^|\\s+)(no-)?'+cs+'(\\s+|$)');

						request = true;
		
				b[c] = b[c].replace( rcs, ' ' );
				// The customizer requires postMessage and CORS (if the site is cross domain)
				b[c] += ( window.postMessage && request ? ' ' : ' no-' ) + cs;
			}());
		</script>
	<!--<![endif]-->
			<div id="wpadminbar" class="nojq nojs">
							<a class="screen-reader-shortcut" href="#wp-toolbar" tabindex="1">Skip to toolbar</a>
						<div class="quicklinks" id="wp-toolbar" role="navigation" aria-label="Toolbar" tabindex="0">
				<ul id="wp-admin-bar-root-default" class="ab-top-menu">
		<li id="wp-admin-bar-wp-logo" class="menupop"><a class="ab-item" aria-haspopup="true" href="http://localhost/C4G/wp-admin/about.php"><span class="ab-icon"></span><span class="screen-reader-text">About WordPress</span></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-wp-logo-default" class="ab-submenu">
		<li id="wp-admin-bar-about"><a class="ab-item" href="http://localhost/C4G/wp-admin/about.php">About WordPress</a>		</li></ul><ul id="wp-admin-bar-wp-logo-external" class="ab-sub-secondary ab-submenu">
		<li id="wp-admin-bar-wporg"><a class="ab-item" href="https://wordpress.org/">WordPress.org</a>		</li>
		<li id="wp-admin-bar-documentation"><a class="ab-item" href="https://codex.wordpress.org/">Documentation</a>		</li>
		<li id="wp-admin-bar-support-forums"><a class="ab-item" href="https://wordpress.org/support/">Support Forums</a>		</li>
		<li id="wp-admin-bar-feedback"><a class="ab-item" href="https://wordpress.org/support/forum/requests-and-feedback">Feedback</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-site-name" class="menupop"><a class="ab-item" aria-haspopup="true" href="http://localhost/C4G/wp-admin/">C4G dev site</a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-site-name-default" class="ab-submenu">
		<li id="wp-admin-bar-dashboard"><a class="ab-item" href="http://localhost/C4G/wp-admin/">Dashboard</a>		</li></ul><ul id="wp-admin-bar-appearance" class="ab-submenu">
		<li id="wp-admin-bar-themes"><a class="ab-item" href="http://localhost/C4G/wp-admin/themes.php">Themes</a>		</li>
		<li id="wp-admin-bar-widgets"><a class="ab-item" href="http://localhost/C4G/wp-admin/widgets.php">Widgets</a>		</li>
		<li id="wp-admin-bar-menus"><a class="ab-item" href="http://localhost/C4G/wp-admin/nav-menus.php">Menus</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-customize" class="hide-if-no-customize"><a class="ab-item" href="http://localhost/C4G/wp-admin/customize.php?url=http%3A%2F%2Flocalhost%2FC4G%2Fprocess-builder-tips-and-tricks%2F">Customize</a>		</li>
		<li id="wp-admin-bar-updates"><a class="ab-item" href="http://localhost/C4G/wp-admin/update-core.php" title="1 Plugin Update"><span class="ab-icon"></span><span class="ab-label">1</span><span class="screen-reader-text">1 Plugin Update</span></a>		</li>
		<li id="wp-admin-bar-comments"><a class="ab-item" href="http://localhost/C4G/wp-admin/edit-comments.php"><span class="ab-icon"></span><span class="ab-label awaiting-mod pending-count count-0" aria-hidden="true">0</span><span class="screen-reader-text">0 comments awaiting moderation</span></a>		</li>
		<li id="wp-admin-bar-new-content" class="menupop"><a class="ab-item" aria-haspopup="true" href="http://localhost/C4G/wp-admin/post-new.php"><span class="ab-icon"></span><span class="ab-label">New</span></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-new-content-default" class="ab-submenu">
		<li id="wp-admin-bar-new-post"><a class="ab-item" href="http://localhost/C4G/wp-admin/post-new.php">Post</a>		</li>
		<li id="wp-admin-bar-new-media"><a class="ab-item" href="http://localhost/C4G/wp-admin/media-new.php">Media</a>		</li>
		<li id="wp-admin-bar-new-page"><a class="ab-item" href="http://localhost/C4G/wp-admin/post-new.php?post_type=page">Page</a>		</li>
		<li id="wp-admin-bar-new-success"><a class="ab-item" href="http://localhost/C4G/wp-admin/post-new.php?post_type=success">Success Stories</a>		</li>
		<li id="wp-admin-bar-new-testimonial"><a class="ab-item" href="http://localhost/C4G/wp-admin/post-new.php?post_type=testimonial">Testimonial</a>		</li>
		<li id="wp-admin-bar-new-team"><a class="ab-item" href="http://localhost/C4G/wp-admin/post-new.php?post_type=team">Team</a>		</li>
		<li id="wp-admin-bar-new-user"><a class="ab-item" href="http://localhost/C4G/wp-admin/user-new.php">User</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-edit"><a class="ab-item" href="http://localhost/C4G/wp-admin/post.php?post=179&#038;action=edit">Edit Post</a>		</li></ul><ul id="wp-admin-bar-top-secondary" class="ab-top-secondary ab-top-menu">
		<li id="wp-admin-bar-search" class="admin-bar-search"><div class="ab-item ab-empty-item" tabindex="-1"><form action="http://localhost/C4G/" method="get" id="adminbarsearch"><input class="adminbar-input" name="s" id="adminbar-search" type="text" value="" maxlength="150" /><label for="adminbar-search" class="screen-reader-text">Search</label><input type="submit" class="adminbar-button" value="Search"/></form></div>		</li>
		<li id="wp-admin-bar-my-account" class="menupop with-avatar"><a class="ab-item" aria-haspopup="true" href="http://localhost/C4G/wp-admin/profile.php">Howdy, eadcock<img alt='' src='http://2.gravatar.com/avatar/509212c064a28864bbb22db0b8367fda?s=26&#038;d=mm&#038;r=g' srcset='http://2.gravatar.com/avatar/509212c064a28864bbb22db0b8367fda?s=52&amp;d=mm&amp;r=g 2x' class='avatar avatar-26 photo' height='26' width='26' /></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-user-actions" class="ab-submenu">
		<li id="wp-admin-bar-user-info"><a class="ab-item" tabindex="-1" href="http://localhost/C4G/wp-admin/profile.php"><img alt='' src='http://2.gravatar.com/avatar/509212c064a28864bbb22db0b8367fda?s=64&#038;d=mm&#038;r=g' srcset='http://2.gravatar.com/avatar/509212c064a28864bbb22db0b8367fda?s=128&amp;d=mm&amp;r=g 2x' class='avatar avatar-64 photo' height='64' width='64' /><span class='display-name'>eadcock</span></a>		</li>
		<li id="wp-admin-bar-edit-profile"><a class="ab-item" href="http://localhost/C4G/wp-admin/profile.php">Edit My Profile</a>		</li>
		<li id="wp-admin-bar-logout"><a class="ab-item" href="http://localhost/C4G/wp-login.php?action=logout&#038;_wpnonce=38ddbffd35">Log Out</a>		</li></ul></div>		</li></ul>			</div>
						<a class="screen-reader-shortcut" href="http://localhost/C4G/wp-login.php?action=logout&#038;_wpnonce=38ddbffd35">Log Out</a>
					</div>

		  </body>
</html>
