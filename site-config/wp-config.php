<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'c4gdev');

/** MySQL database username */
define('DB_USER', 'c4gadmin');

/** MySQL database password */
define('DB_PASSWORD', 'y0uRm1ssion1216');

/** MySQL hostname */
define('DB_HOST', 'c4g.eadcock-devsite.com');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'WfR=}jmZMq,JpJ[dj.gf&Cyq8^hFFNdA&H<pBzEos]P&w%@h!lm~j1K4Lhqp`P67');
define('SECURE_AUTH_KEY',  '$r;pX=L$xfuu;Ni+-G@>|>i^36YPj/nL6/$/9e9:y4Kl;O|1jSPA2ah_!j.6K6`B');
define('LOGGED_IN_KEY',    '*j`q|BSQPr(ZuW}l#2,_WzGEpL`$5VDQ!E/CJZ#OS2.)=.pu^_-{+_IcDK3[XAjk');
define('NONCE_KEY',        ':[g87l/mk f=bQU0k]hP5khP,$fcRwxL]1IPg8ck;89op*$Y9S-_!R=pSEw14.kl');
define('AUTH_SALT',        '8o1qhab:5qb|pQ>ASFNZAT],o&$&:Tr5YcM^=YQLu~][7h8#jf c@V=PcHSvYwui');
define('SECURE_AUTH_SALT', 'O/z>4)hS-5ov,a%d${Iupya;o#I@D{Iy9-/b_j&YIJxYj,D5TAQyqlyN I5Q++X}');
define('LOGGED_IN_SALT',   '7M)HZqrmHpwh#<9n84Zz6{nN. >5NIUo*`CgzCsW,pMk`sUNaZ2qt@`s/l>kA5Ef');
define('NONCE_SALT',       'cyDZb}BAPDD&{OfBhJt_zW^*y8at[~c<otdLeh2uJg%Y2K)rrXj~!G33RT}`p,U!');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'c4g_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define('FS_METHOD', 'direct'); /*delete this line before going live!!!*/

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
