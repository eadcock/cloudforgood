<?php

class Luxe_Custom_Post_Types {

    public function __construct( $custom_post_types = array() ) {
        foreach ($custom_post_types as $custom_post_type) {
            add_action( 'init', array( $this, $custom_post_type . '_init' ), 1 );
        }
    }

    /**
     * Success Story
     */
    function successstories_init() {
    	$labels = array(
    		'name'               => esc_attr__( 'Success Stories', 'luxe-text-domain' ),
    		'singular_name'      => esc_attr__( 'Success Story', 'luxe-text-domain' ),
    		'menu_name'          => esc_attr__( 'Success Stories', 'luxe-text-domain' ),
    		'name_admin_bar'     => esc_attr__( 'Success Stories', 'luxe-text-domain' ),
    		'add_new'            => esc_attr__( 'New Success Story', 'luxe-text-domain' ),
    		'add_new_item'       => esc_attr__( 'New Success Story', 'luxe-text-domain' ),
    		'new_item'           => esc_attr__( 'New Success Story', 'luxe-text-domain' ),
    		'edit_item'          => esc_attr__( 'Edit Success Story', 'luxe-text-domain' ),
    		'view_item'          => esc_attr__( 'View Success Story', 'luxe-text-domain' ),
    		'all_items'          => esc_attr__( 'All Success Stories', 'luxe-text-domain' ),
    		'search_items'       => esc_attr__( 'Search Success Stories', 'luxe-text-domain' ),
    		'parent_item_colon'  => esc_attr__( 'Parent Success Stories:', 'luxe-text-domain' ),
    		'not_found'          => esc_attr__( 'No success stories found.', 'luxe-text-domain' ),
    		'not_found_in_trash' => esc_attr__( 'No success stories found in Trash.', 'luxe-text-domain' )
    	);

    	$args = array(
    		'labels'             => $labels,
            'description'        => esc_attr__( 'Organize and show off your client successes.', 'luxe-text-domain' ),
    		'public'             => true,
    		'publicly_queryable' => true,
    		'show_ui'            => true,
    		'show_in_menu'       => true,
    		'query_var'          => true,
    		'rewrite'            => array( 'slug' => $this->get_successstories_slug() ),
    		'capability_type'    => 'post',
    		'has_archive'        => true,
    		'hierarchical'       => false,
    		'menu_position'      => null,
    		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'post-formats' ),
    	);
    	register_post_type( 'successstories', $args );

        register_taxonomy( 'successstories_category', array('successstories'), array(
            'hierarchical' => true, 
            'label' => 'Categories', 
            'singular_label' => 'Category', 
            'rewrite' => array( 'slug' => 'category', 'with_front'=> false )
            )
        );
        register_taxonomy_for_object_type( 'successstories_category', 'successstories' );
    }

    /**
     * Get success slug for URL
     * @return str
     */
    function get_successstories_slug() {
        return get_theme_mod('successstories_slug', 'successstories');
    }

    /**
     * Team
     */
    function team_init() {
    	$labels = array(
    		'name'               => esc_attr__( 'Team', 'luxe-text-domain' ),
    		'singular_name'      => esc_attr__( 'Team Member', 'luxe-text-domain' ),
    		'menu_name'          => esc_attr__( 'Team', 'luxe-text-domain' ),
    		'name_admin_bar'     => esc_attr__( 'Team', 'luxe-text-domain' ),
    		'add_new'            => esc_attr__( 'New Team Member', 'luxe-text-domain' ),
    		'add_new_item'       => esc_attr__( 'New Team Member', 'luxe-text-domain' ),
    		'new_item'           => esc_attr__( 'New Team Member', 'luxe-text-domain' ),
    		'edit_item'          => esc_attr__( 'Edit Team Member', 'luxe-text-domain' ),
    		'view_item'          => esc_attr__( 'View Team Member', 'luxe-text-domain' ),
    		'all_items'          => esc_attr__( 'All Team Members', 'luxe-text-domain' ),
    		'search_items'       => esc_attr__( 'Search Team', 'luxe-text-domain' ),
    		'parent_item_colon'  => esc_attr__( 'Parent Team:', 'luxe-text-domain' ),
    		'not_found'          => esc_attr__( 'No team members found.', 'luxe-text-domain' ),
    		'not_found_in_trash' => esc_attr__( 'No team members found in Trash.', 'luxe-text-domain' )
    	);

    	$args = array(
    		'labels'             => $labels,
            'description'        => esc_attr__( 'Show off a team members skills.', 'luxe-text-domain' ),
    		'public'             => true,
    		'publicly_queryable' => true,
    		'show_ui'            => true,
    		'show_in_menu'       => true,
    		'query_var'          => true,
    		'rewrite'            => array( 'slug' => $this->get_team_slug() ),
    		'capability_type'    => 'post',
    		'has_archive'        => true,
    		'hierarchical'       => false,
    		'menu_position'      => null,
    		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'post-formats' ),
    	);
    	register_post_type( 'team', $args );

        register_taxonomy( 'team_category', array('team'), array(
            'hierarchical' => true, 
            'label' => 'Categories', 
            'singular_label' => 'Category', 
            'rewrite' => array( 'slug' => 'category', 'with_front'=> false )
            )
        );
        register_taxonomy_for_object_type( 'team_category', 'team' );
    }
    
    

    /**
     * Get team slug for URL
     * @return str
     */
    function get_team_slug() {
        return get_theme_mod('team_slug', 'team');
    }

 

    /**
     * Testimonials
     */
    function testimonials_init() {
        $labels = array(
            'name'               => esc_attr__( 'Testimonial', 'luxe-text-domain' ),
            'singular_name'      => esc_attr__( 'Testimonial', 'luxe-text-domain' ),
            'menu_name'          => esc_attr__( 'Testimonial', 'luxe-text-domain' ),
            'name_admin_bar'     => esc_attr__( 'Testimonial', 'luxe-text-domain' ),
            'add_new'            => esc_attr__( 'New Testimonial', 'luxe-text-domain' ),
            'add_new_item'       => esc_attr__( 'New Testimonial', 'luxe-text-domain' ),
            'new_item'           => esc_attr__( 'New Testimonial', 'luxe-text-domain' ),
            'edit_item'          => esc_attr__( 'Edit Testimonial', 'luxe-text-domain' ),
            'view_item'          => esc_attr__( 'View Testimonial', 'luxe-text-domain' ),
            'all_items'          => esc_attr__( 'All Testimonial Item', 'luxe-text-domain' ),
            'search_items'       => esc_attr__( 'Search Testimonials', 'luxe-text-domain' ),
            'parent_item_colon'  => esc_attr__( 'Parent Testimonial:', 'luxe-text-domain' ),
            'not_found'          => esc_attr__( 'No testimonial items found.', 'luxe-text-domain' ),
            'not_found_in_trash' => esc_attr__( 'No testimonial items found in Trash.', 'luxe-text-domain' )
        );

        $args = array(
            'labels'             => $labels,
            'description'        => esc_attr__( 'Organize and show off your work publicly by adding testimonial items.', 'luxe-text-domain' ),
            'public'             => false,
            'publicly_queryable' => false,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'capability_type'    => 'post',
            'has_archive'        => false,
            'hierarchical'       => false,
            'menu_position'      => null,
            'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ),
        );
        register_post_type( 'testimonial', $args );

    }

}