<?php

use Luxe\Helper;

/**
 * Luxe progress bar VC map
 */

function luxe_progress_bar_vc() {
    vc_map(
    	array(
            'name' => esc_attr__( 'Progress Bar', 'luxe-text-domain' ),
            'base' => 'luxe_progress_bar',
            'icon' => 'icon-wpb-graph',
            'is_container' => true,
            'show_settings_on_create' => false,
            'category' => esc_attr__( 'Content', 'luxe-text-domain' ),
            'description' => esc_attr__( 'Collapsible content panels', 'luxe-text-domain' ),
            'params' => array(
                array(
                    'type' => 'textfield',
                    'param_name' => 'label',
                    'heading' => esc_attr__( 'Progress Bar Label', 'luxe-text-domain' ),
                    'description' => esc_attr__( 'Enter the text displayed above your progress bar.', 'luxe-text-domain' ),
                    "admin_label" => true,    
                ),
                array(
                    'type' => 'textfield',
                    'param_name' => 'percentage',
                    'heading' => esc_attr__( 'Progress Percentage', 'luxe-text-domain' ),
                    'description' => esc_attr__( 'Enter an integer between 0-100.', 'luxe-text-domain' ),
                ),
                array(
                    "type"       => "colorpicker",
                    "class"      => "",
                    "heading"    => esc_attr__("Label Color", "luxe-text-domain"),
                    "param_name" => "label_color",
                    "description" => esc_attr__("Color for label text.", "luxe-text-domain"),
                ),
                array(
                    "type"       => "colorpicker",
                    "class"      => "",
                    "heading"    => esc_attr__("Bar Background Color", "luxe-text-domain"),
                    "param_name" => "bar_bg_color",
                    "description" => esc_attr__("Color behind your progress bar.", "luxe-text-domain"),
                ),
                array(
                    "type"       => "colorpicker",
                    "class"      => "",
                    "heading"    => esc_attr__("Bar Color", "luxe-text-domain"),
                    "param_name" => "bar_color",
                    "description" => esc_attr__("Primary color of your progress bar.", "luxe-text-domain"),
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_attr__( 'Extra class name', 'luxe-text-domain' ),
                    'param_name' => 'el_class',
                    'description' => esc_attr__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'luxe-text-domain' ),
                ),
                array(
                    'type' => 'css_editor',
                    'heading' => esc_attr__( 'CSS box', 'luxe-text-domain' ),
                    'param_name' => 'css',
                    'group' => esc_attr__( 'Design Options', 'luxe-text-domain' ),
                ),
            ),
        )
    );
}
add_action( 'vc_before_init', 'luxe_progress_bar_vc', 100 );

/**
 * Luxe progress bar
 */
function luxe_progress_bar($atts, $content = null)
{
    extract(shortcode_atts( array(
        'percentage'=>'',
        'label'=>'',
        'label_color'=>'',
        'bar_color'=>'',
        'bar_bg_color'=>'',
        'el_class'=>'',
        'css' => '',
    ),$atts));           

    // Progress bar progress inline CSS
    $label_inline_css = 'style="';
    if ($label_color) {
        $label_inline_css .= 'color:' . $label_color . ';';
    }
    $label_inline_css .= '"';
    // Progress bar inline CSS
    $progress_bar_inline_css = 'style="';
    if ($bar_bg_color) {
        $progress_bar_inline_css .= 'background-color:' . $bar_bg_color . ';';
    }
    $progress_bar_inline_css .= '"';
    // Progress bar progress inline CSS
    $progress_bar_progress_inline_css = 'style="';
    if ($bar_color) {
        $progress_bar_progress_inline_css .= 'background-color:' . $bar_color . ';';
    }
    if ($percentage) {
        $progress_bar_progress_inline_css .= 'width:' . $percentage . '%;';
    }
    $progress_bar_progress_inline_css .= '"';

    // Design option CSS class
    $css_class = Helper\get_vc_class($css, $atts);

    ob_start();
    ?>
        <div class="progress wpb_content_elemnt-half <?php echo $el_class; ?> <?php echo $css_class; ?>">
            <small class="progress-label" <?php echo $label_inline_css; ?>><?php echo $label; ?></small>
            <div class="progress-bar" <?php echo $progress_bar_inline_css; ?>>
                <div class="progress-bar-progress has-animation" data-animation="transition.slideLeftBigIn" data-animation-delay="300" data-animation-duration="500" <?php echo $progress_bar_progress_inline_css; ?>></div>
            </div>
        </div>           
    <?php
    $output = ob_get_clean();
    return $output;     
}
add_shortcode( 'luxe_progress_bar', 'luxe_progress_bar' );
