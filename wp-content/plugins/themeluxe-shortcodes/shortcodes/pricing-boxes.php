<?php

use Luxe\Helper;

function luxe_pricing_vc() {
	vc_map(
	array(
	   "name" => esc_attr__("Pricing Box","luxe-text-domain"),
	   "base" => "luxe_pricing",
	   "class" => "luxe_pricing",
	   "icon" => "luxe_pricing",
	   "category" => "Content",
	   "description" => esc_attr__("Create pricing tables.","luxe-text-domain"),
	   "params" => array(
			// array(
			// 	"type" => "colorpicker",
			// 	"class" => "",
			// 	"heading" => esc_attr__("Background Color", "luxe-text-domain"),
			// 	"param_name" => "bg_color",
			// 	"value" => "",
			// 	"description" => esc_attr__("Select normal background color.", "luxe-text-domain"),
			// ),
			// array(
			// 	"type" => "colorpicker",
			// 	"class" => "",
			// 	"heading" => esc_attr__("Hover background Color", "luxe-text-domain"),
			// 	"param_name" => "bg_color_hover",
			// 	"value" => "",
			// 	"description" => esc_attr__("Select highlight background color.", "luxe-text-domain"),
			// 	"dependency" => Array("element" => "color_bg_main"),
			// ),
			// array(
			// 	"type" => "colorpicker",
			// 	"class" => "",
			// 	"heading" => esc_attr__("Hover text Color", "luxe-text-domain"),
			// 	"param_name" => "color_hover",
			// 	"value" => "",
			// 	"description" => esc_attr__("Select highlight background color.", "luxe-text-domain"),
			// 	"dependency" => Array("element" => "color_scheme","value" => array("custom")),
			// ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_attr__("Price", "luxe-text-domain"),
                "param_name" => "price",
                "value" => "",
                "description" => esc_attr__("Enter the price for this package. e.g. 157", "luxe-text-domain"),
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_attr__("Price Unit", "luxe-text-domain"),
                "param_name" => "price_unit",
                "value" => "",
                "description" => esc_attr__("Enter the unit for the price. e.g. $", "luxe-text-domain"),
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_attr__("Price Subheading", "luxe-text-domain"),
                "param_name" => "price_subheading",
                "value" => "",
                "description" => esc_attr__("Enter the price subheading. e.g. per month", "luxe-text-domain"),
            ),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_attr__("Package Name / Title", "luxe-text-domain"),
				"param_name" => "heading",
				"admin_label" => true,
				"value" => "",
				"description" => esc_attr__("Enter the package name or table heading", "luxe-text-domain"),
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_attr__("Subheading", "luxe-text-domain"),
				"param_name" => "subheading",
				"value" => "",
				"description" => esc_attr__("Enter short description for this package", "luxe-text-domain"),
			),
			array(
				"type" => "textarea_html",
				"class" => "",
				"heading" => esc_attr__("Features", "luxe-text-domain"),
				"param_name" => "content",
				"value" => "",
				"description" => esc_attr__("Create the features list using un-ordered list elements.", "luxe-text-domain"),
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_attr__("Button Text", "luxe-text-domain"),
				"param_name" => "button_text",
				"value" => "",
				"description" => esc_attr__("Enter call to action button text", "luxe-text-domain"),
			),
			array(
				"type" => "vc_link",
				"class" => "",
				"heading" => esc_attr__("Button Link", "luxe-text-domain"),
				"param_name" => "button_link",
				"value" => "",
				"description" => esc_attr__("Select / enter the link for call to action button", "luxe-text-domain"),
			),
			// array(
			// 	"type" => "checkbox",
			// 	"class" => "",
			// 	"heading" => "",
			// 	"param_name" => "featured",
			// 	"value" => array("Make this pricing box featured" => "enable"),
			// ),
            array(
                'type' => 'dropdown',
                'heading' => esc_attr__( 'Alignment', 'luxe-text-domain' ),
                'param_name' => 'text_align',
                'value' => array(
                    esc_attr__( 'Left', 'luxe-text-domain' ) => 'text-align-left',
                    esc_attr__( 'Center', 'luxe-text-domain' ) => 'text-align-center',
                    esc_attr__( 'Right', 'luxe-text-domain' ) => 'text-align-right',
                ),
                'description' => esc_attr__( 'Select text alignment.', 'luxe-text-domain' ),
            ),
			array(
					"type" => "textfield",
					"heading" => esc_attr__("Extra class name", "luxe-text-domain"),
					"param_name" => "el_class",
					"description" => esc_attr__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "luxe-text-domain")
			),
			/* typoraphy - price */
			array(
				"type" => "textfield",
				"class" => "font-size",
				"heading" => esc_attr__("Price Font Size", "luxe-text-domain"),
				"param_name" => "price_font_size",
				"min" => 10,
				"suffix" => "px",
				"group" => "Typography"
			),
            array(
                'type' => 'css_editor',
                'heading' => esc_attr__( 'Css', 'luxe-text-domain' ),
                'param_name' => 'css',
                'group' => esc_attr__( 'Design options', 'luxe-text-domain' ),
            ),
		)// params
	));// vc_map
}
add_action( 'vc_before_init', 'luxe_pricing_vc', 100 );


function luxe_pricing( $atts, $content = null ) {

    extract( shortcode_atts( array(
        'heading' => '',
        'subheading' => '',
        'price' => '',
        'price_unit' => '',
        'price_font_size' => '',
        'price_subheading' => '',
        'button_link' => '',
        'button_text' => '',
        'el_class' => '',
        'text_align' => '',
        'css' => '',
    ), $atts ) );

    // Design option CSS class
    $css_class = Helper\get_vc_class($css, $atts);

    ob_start();
    ?>
        <div class="pricing-box <?php echo $el_class; ?> <?php echo $text_align; ?> <?php echo $css_class; ?>wpb_content_element">
            <div class="pricing-box-price h1" style="font-size:<?php echo $price_font_size; ?>">
                <sup class="pricing-box-unit"><?php echo $price_unit; ?></sup>
                <?php echo $price; ?>
            </div>
            <div class="pricing-box-price-subheading h5"><?php echo $price_subheading; ?></div>
            <h3 class="pricing-box-heading h4"><?php echo $heading; ?></h3>
            <div class="pricing-box-subheading h5"><?php echo $subheading; ?></div>
            <div class="pricing-box-content"><?php echo $content; ?></div>
            <?php if ($button_text && $button_link) { ?>
                <a href="<?php echo $button_link; ?>" class="btn btn-default"><?php echo $button_text; ?></a>
            <?php } ?>
        </div>
    <?php
    $output = ob_get_clean(); 
    return $output;

}
add_shortcode( 'luxe_pricing', 'luxe_pricing' );