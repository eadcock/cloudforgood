<?php

use Luxe\Helper;

/**
 * Luxe infobox VC map
 */

function luxe_infobox_vc() {
    vc_map(
    	array(
    	   "name" => esc_attr__("Info Box","luxe-text-domain"),
    	   "base" => "luxe_infobox",
    	   "class" => "luxe_infobox",
    	   "icon" => "infobox",
    	   "category" => "Content",
    	   "description" => esc_attr__("A box with an icon that displays features or information.","luxe-text-domain"),
    	   "params" => array(
    			array(
    				"type" => "dropdown",
    				"class" => "",
    				"heading" => esc_attr__("Icon to display:", "luxe-text-domain"),
    				"param_name" => "icon_type",
    				"value" => array(
                        esc_attr__("Font Icon","luxe-text-domain") => "selector",
                        esc_attr__("Custom Image Icon","luxe-text-domain") => "custom",
                        esc_attr__("No Icon","luxe-text-domain") => "none",
    				),
    				"description" => esc_attr__("Use an existing font icon or upload a custom image.  Leave blank for none.", "luxe-text-domain")
    			),
    			array(
    				"type" => "iconpicker",
    				"class" => "",
    				"heading" => esc_attr__("Select Icon ","luxe-text-domain"),
                    'settings'    => array(
                        'emptyIcon'    => false,
                        'type'         => 'ionicons',
                        'iconsPerPage' => 200,
                    ),
    				"param_name" => "icon",
    				"value" => "",
    				"description" => esc_attr__("Click and select icon of your choice. If you can't find the one that suits for your purpose","luxe-text-domain").", ".esc_attr__("you can","luxe-text-domain")." <a href='admin.php?page=font-icon-Manager' target='_blank'>".esc_attr__("add new here","luxe-text-domain")."</a>.",
    				"dependency" => Array("element" => "icon_type","value" => array("selector")),
    			),
    			array(
    				"type" => "attach_image",
    				"class" => "",
    				"heading" => esc_attr__("Upload Image", "luxe-text-domain"),
    				"param_name" => "icon_img",
    				"value" => "",
    				"description" => esc_attr__("Upload the custom image icon.", "luxe-text-domain"),
    				"dependency" => Array("element" => "icon_type","value" => array("custom")),
    			),
                array(
                    "type" => "textfield",
                    "class" => "",
                    "heading" => esc_attr__("Size of Icon", "luxe-text-domain"),
                    "param_name" => "icon_size",
                    "value" => 32,
                    "min" => 12,
                    "max" => 72,
                    "suffix" => "px",
                    "description" => esc_attr__("Enter value in pixels.", "luxe-text-domain"),
                    "dependency" => Array("element" => "icon_type","value" => array("selector")),
                ),
    			array(
    				"type" => "colorpicker",
    				"class" => "",
    				"heading" => esc_attr__("Icon Color", "luxe-text-domain"),
    				"param_name" => "icon_color",
    				"value" => "",
    				"dependency" => Array("element" => "icon_type","value" => array("selector")),
    			),
    		  array(
    			 "type" => "dropdown",
    			 "class" => "",
    			 "heading" => esc_attr__("Icon Position", "luxe-text-domain"),
    			 "param_name" => "icon_position",
    			 "value" => array(
    			 		esc_attr__('Top','luxe-text-domain') => 'top',
    			 		esc_attr__('Top Center','luxe-text-domain') => 'top-center',
    					esc_attr__('Right','luxe-text-domain') => 'right',
    					esc_attr__('Left','luxe-text-domain') => 'left',	
    			 		),							
    			 "description" => esc_attr__("Enter Position of Icon", "luxe-text-domain")
    			 ),
              array(
                 "type" => "textfield",
                 "class" => "",
                 "heading" => esc_attr__("Heading", "luxe-text-domain"),
                 "param_name" => "heading",
                 "admin_label" => true,
                 "value" => "",
                 "description" => esc_attr__("Enter the main heading.", "luxe-text-domain")
              ),
                array(
                 "type" => "textarea_html",
                 "class" => "",
                 "heading" => esc_attr__("Text", "luxe-text-domain"),
                 "param_name" => "content",
                 "value" => "",
                 "description" => esc_attr__("Enter the text shown beneath your heading.  Leave blank for none.", "luxe-text-domain")
                ),
              array(
                    "type" => "textfield",
                    "class" => "",
                    "heading" => esc_attr__("Heading Font Size", "luxe-text-domain"),
                    "param_name" => "heading_size",
                    "suffix" => "px",
                    "description" => esc_attr__("Enter value in pixels.", "luxe-text-domain")
                ),
              array(
                "type" => "colorpicker",
                "class" => "",
                "heading" => esc_attr__("Heading Color", "luxe-text-domain"),
                "param_name" => "heading_color",
                "value" => "#333333",
              ),
              array(
                "type" => "vc_link",
                "class" => "",
                "heading" => esc_attr__("Link Infobox", "luxe-text-domain"),
                "param_name" => "link",
                "description" => esc_attr__("Link the entire infobox to a given URL or page.  Leave the URL blank for no link.", "luxe-text-domain")
              ),
              array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_attr__("Extra Class",  "luxe-text-domain"),
                "param_name" => "el_class",
                "value" => "",
                "description" => esc_attr__("Add extra class name that will be applied to the icon process, and you can use this class for your customizations.",  "luxe-text-domain"),
              ),
              array(
                  'type' => 'css_editor',
                  'heading' => esc_attr__( 'CSS box', 'luxe-text-domain' ),
                  'param_name' => 'css',
                  'group' => esc_attr__( 'Design Options', 'luxe-text-domain' ),
              ),
    		),
    	)
    );
}
add_action( 'vc_before_init', 'luxe_infobox_vc', 100 );

/**
 * Luxe infobox
 */
function luxe_infobox($atts, $content = null)
{
    extract(shortcode_atts( array(
        'icon_type' => '',
        'icon' => '',
        'icon_img' => '',
        'img_width' => '',
        'icon_size' => '',              
        'icon_color' => '',
        'heading' => '',              
        'heading_size' => '',              
        'heading_color' => '',
        'link' => '',
        'icon_position'=>'top',
        'speed'=>'',
        'el_class'=>'',
        'css' => '',
    ),$atts));           

    // Icon inline CSS
    $icon_inline_css = 'style="';
    if (!empty($icon_size)) {
        $icon_inline_css .= 'font-size:'.$icon_size.'px;';
        if ($icon_position == 'left' || $icon_position == 'right') {
            $icon_inline_css .= 'width:'.$icon_size.'px;';
        }
    }
    if (!empty($icon_color)) {
        $icon_inline_css .= 'color:' . $icon_color . ';';
    }
    $icon_inline_css .= '"';
    // Heading inline CSS
    $heading_inline_css = 'style="';
    if (!empty($heading_size)) {
        $heading_inline_css .= 'font-size:'.$heading_size.'px;';
    }
    if (!empty($heading_color)) {
        $heading_inline_css .= 'color:' . $heading_color . ';';
    }
    $heading_inline_css .= '"';

    // Design option CSS class
    $css_class = Helper\get_vc_class($css, $atts);

    $icon_class = 'icon-position-'.$icon_position;
    $id = uniqid();
    $link = Helper\build_link($link);
    ob_start();
    ?>
    <div class="infobox infobox-<?php echo $icon_position; ?> wpb_content_element <?php echo esc_attr( $css_class ); ?> <?php echo $el_class; ?>">
        <?php if ($link['url']) { ?>
            <a href="<?php echo $link['url']; ?>" title="<?php echo $link['title']; ?>" target="<?php echo $link['target']; ?>">
        <?php } ?>
        <?php if ((!empty($icon) || !empty($icon_img)) && $icon_type != 'none') { ?>
            <div class="infobox-icon <?php echo $icon_class; ?>">
                <?php if ($icon_type == 'custom' && $icon_img) { ?>
                    <img src="<?php $image = wp_get_attachment_image_src( $icon_img, 'full' ); echo $image[0]; ?>">
                <?php } else { ?>
                    <i class="icon <?php echo $icon;?>" <?php echo $icon_inline_css; ?>></i>
                <?php } ?>
            </div>
        <?php } ?>
        <div class="infobox-main">
            <div class="infobox-heading h4" <?php echo $heading_inline_css; ?>><?php echo $heading; ?></div>
            <div class="infobox-content"><?php echo $content; ?></div>
        </div>
        <?php if ($link['url']) { ?></a><?php } ?>
    </div>           
    <?php
    $output = ob_get_clean();
    return $output;     
}
add_shortcode( 'luxe_infobox', 'luxe_infobox' );
