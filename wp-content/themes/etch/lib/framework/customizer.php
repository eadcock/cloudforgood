<?php

namespace Luxe\Customizer;

use Luxe\Assets;

/**
 * Customizer JS
 */
function customize_preview_js() {
  wp_enqueue_script('sage/customizer', Assets\luxe_asset_path('scripts/customizer.js'), array('customize-preview'), null, true);
}
add_action('customize_preview_init', __NAMESPACE__ . '\\customize_preview_js');
