<?php

LuxeOption::add_section( 'page_headers', array(
    'title'          => esc_attr__( 'Page Headers', 'etch' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
    'panel'          => 'body',
) );

/**
 * Page Titles
 */
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'switch',
    'settings'    => 'page_headers',
    'label'       => esc_attr__( 'Page Headers', 'etch' ),
    'description' => esc_attr__( 'Turn on or off all page headers by default.', 'etch' ),
    'help'        => esc_attr__( 'This can be overriden on each individual page.', 'etch' ),
    'section'     => 'page_headers',
    'default'     => false,
    'priority'    => 10,
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color',
    'settings'    => 'page_header_typography_color',
    'label'       => esc_attr__( 'Page Header Font Color', 'etch' ),
    'section'     => 'page_headers',
    'default'     => '#fff',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.page-header, .page-header a, .page-header h1, .page-header .subtitle, .page-header div',
            'property' => 'color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => '.page-header, .page-header a, .page-header h1, .page-header .subtitle, .page-header div',
            'property' => 'color',
            'function' => 'css',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'radio-buttonset',
    'settings'    => 'page_header_alignment',
    'label'       => esc_attr__( 'Page Header Alignment', 'etch' ),
    'description' => esc_attr__( 'Choose how your text is aligned by default in your header.', 'etch' ),
    'section'     => 'page_headers',
    'default'     => 'left',
    'priority'    => 10,
    'choices'     => array(
        'left'   => 'left',
        'center' => 'center',
        'right'  => 'right',
    ),
    'output' => array(
        array(
            'element' => '.page-header',
            'function' => 'css',
            'property' => 'text-align',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => '.page-header',
            'property' => 'text-align',
            'function' => 'css',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color',
    'settings'    => 'page_header_bg_color',
    'label'       => esc_attr__( 'Page Header Background Color', 'etch' ),
    'section'     => 'page_headers',
    'default'     => '#1e1e1e',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.page-header',
            'property' => 'background-color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => '.page-header',
            'property' => 'background-color',
            'function' => 'css',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'dimension',
    'settings'    => 'page_header_padding_top',
    'label'       => esc_attr__( 'Page Header Top Padding', 'etch' ),
    'description' => esc_attr__( 'The padding above your default page header.', 'etch' ),
    'section'     => 'page_headers',
    'default'     => '200px',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.page-header',
            'property' => 'padding-top',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => '.page-header',
            'property' => 'padding-top',
            'function' => 'css',
        ),
    ),
    'choices' => array(
        'units' => array( 'px', '%' )
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'dimension',
    'settings'    => 'page_header_padding_bottom',
    'label'       => esc_attr__( 'Page Header Bottom Padding', 'etch' ),
    'description' => esc_attr__( 'The padding below your default page header.', 'etch' ),
    'section'     => 'page_headers',
    'default'     => '100px',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.page-header',
            'property' => 'padding-bottom',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => '.page-header',
            'property' => 'padding-bottom',
            'function' => 'css',
        ),
    ),
    'choices' => array(
        'units' => array( 'px', '%' )
    ),
) );
