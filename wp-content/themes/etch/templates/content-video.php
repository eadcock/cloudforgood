<?php use Luxe\Extras; ?>
<?php 
    global $image_size;
    $image_size = isset($image_size) ? $image_size : 'full'; 
?>
<article <?php post_class(); ?>>
    <header>
        <div class="featured-content">
            <?php $div_id = uniqid(); ?>
            <a href="<?php the_permalink(); ?>">
                <?php the_post_thumbnail( $image_size ); ?>
            </a>
            <div class="post-overlay">
                <a href="<?php the_permalink(); ?>" class="full-link"></a>
                <i class="icon ion-ios-play icon-play-video"></i>
            </div>
        </div>
        <div class="categories h5"><?php the_category(', '); ?></div>        
        <h2 class="entry-title h3"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    </header>
    <div class="entry-summary">
        <?php the_excerpt(); ?>
    </div>
</article> 
