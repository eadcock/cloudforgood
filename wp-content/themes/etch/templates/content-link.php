<?php use Luxe\Extras; ?>
<article <?php post_class(); ?>>
    <header>
        <i class="icon ion-link icon-link"></i>
        <h2 class="entry-title h3"><a href="<?php echo get_post_meta( get_the_ID(), '_post_content_link', true ); ?>" target="_blank"><?php the_title(); ?></a></h2>
    </header>
    <div class="entry-summary">
        <?php the_excerpt(); ?>
    </div>
</article>
