<?php 
    use Luxe\Extras;
    use Luxe\Elements; 
?>
<?php while (have_posts()) : the_post(); ?>
<article <?php post_class('content-single-team'); ?>>
    <div class="container-fluid post-noheader">
    <div class="page-header-overlay">
        
        <div class="row">
            
            </div>
        <div class="row">
            <div class="entry-content col-md-4">
                <div class="entry-content-inner">

                    <?php the_content(); ?>                
                    <div class="social-share-wrap">
                        <?php 
                            //echo Extras\social_share(get_the_ID(), get_the_title(), true); 
                        ?>  
                    </div>
                </div>
            </div>
            <div class="featured-content col-md-8 pull-right">
                <div class="featured-content-inner">
                    <?php
                        if (!empty(get_post_meta( get_the_ID(), '_team_content_custom', true ))) {
                            echo get_post_meta( get_the_ID(), '_team_content_custom', true );
                        } else {
                            echo get_template_part('templates/post-format',  get_post_format());
                        }
                    ?>
                </div>
            </div>
            
        </div>
    </div>
        
    <footer>
        <?php get_template_part('templates/single-nav'); ?>
        <?php //get_template_part('templates/author-meta'); ?>
        <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . esc_html__('Pages:', 'etch'), 'after' => '</p></nav>']); ?>
    </footer>
    <?php //comments_template('/templates/comments.php'); ?>
        </div>
</article>
<?php endwhile; ?>
