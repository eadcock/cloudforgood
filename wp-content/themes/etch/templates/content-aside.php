<?php use Luxe\Extras; ?>
<article <?php post_class(''); ?>>
    <header>
        <h2 class="entry-title h3"><?php the_title(); ?></h2>
    </header>
    <div class="entry-summary">
        <?php the_excerpt(); ?>
    </div>
</article>
