<?php use Luxe\Elements; ?>
<nav id="nav-primary" class="nav-primary off-canvas off-canvas-overlay nav-side" data-move-x="0" data-move-y="" data-active-class="nav-active">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="col-sm-12"><h4 class="nav-side-title"><?php echo do_shortcode(get_theme_mod( 'side_nav_content', '' )); ?></h4></div>
                <?php
                    if (has_nav_menu('primary_navigation')) :
                        wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'navbar-nav nav'));
                    endif;
                ?>
            </div>
            <div class="col-md-6">
                <?php dynamic_sidebar('sidebar-header'); ?>
            </div>
        </div>
    </div>
</nav>
<header class="banner header-nav-btn-right" id="header">
    <div class="container">
        <div class="pull-left brand-pull">
            <?php get_template_part( 'templates/headers/brand' ); ?>
        </div>
        <div class="pull-right">
            <?php echo Elements\nav_button(); ?>
<!--             <div class="nav-btn-wrap close-btn-wrap">
                <div class="toggle-off-canvas close-btn nav-btn" data-offcanvas-target="#nav-primary">
                    <i class="icon ion-android-close"></i>
                </div>
            </div> -->
        </div>
        <?php echo Elements\header_buttons(); ?>
    </div>
</header>
