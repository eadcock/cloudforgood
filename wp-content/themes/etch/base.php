<?php

use Luxe\Setup;
use Luxe\Wrapper;

?>
<!doctype html>
<html <?php language_attributes(); ?>>
    <?php get_template_part('templates/head'); ?>
    <body <?php body_class(); ?>>
    <?php get_template_part( 'templates/border' ); ?>
    <?php
        do_action('luxe_get_header');
        get_template_part('templates/header');
    ?>
    <?php get_template_part( 'templates/loading' ); ?>
    <div id="shadow-overlay" class="shadow-overlay"></div>
    <?php do_action('luxe_before_content_wrapper'); ?>
    <div id="content-wrapper" class="content-wrapper">

        <?php do_action('luxe_before_page_header'); ?>
        <?php get_template_part('templates/page', 'header'); ?>
        <?php do_action('luxe_after_page_header'); ?>

        <?php do_action('luxe_before_content_container'); ?>
        <div class="<?php echo implode( ' ', apply_filters('luxe_content_container_class', array('wrap', 'container')) ) ; ?>" role="document" id="content">
            <?php do_action('luxe_before_content'); ?>
            <div class="row">
                <main class="main">
                    <?php include Wrapper\template_path(); ?>
                </main><!-- /.main -->
                <?php get_template_part( 'sidebar' ); ?>
            </div><!-- /.row -->
        </div><!-- /.wrap -->
        <?php do_action('luxe_after_content_container'); ?>
    </div><!-- /.content-wrapper -->
    <?php
        do_action('luxe_get_footer');
        get_template_part('templates/footer');
        wp_footer();
    ?>
  </body>
</html>
